var express = require('express');
var router = express.Router();

require('dotenv').config()


const Page_Login = function(req, res) {
    res.render('Dangnhap.ejs');
};

const Page_Danhba = function(req, res) {
    res.render('Danhba.ejs');
};
const Page_Chat = function(req, res) {
    res.render('chat.ejs');
};
const Page_Dangki = function(req, res) {
    res.render('DangkiTaikhoan.ejs');
};
const Page_OTP = function(req, res) {
    res.render('otp.ejs');
}
const Page_newUser = function(req, res) {
    res.render('thongtindangki.ejs')
}
const Page_Danhsachnguoidung = function(req, res) {
    res.render('test.ejs')
}

module.exports = {
    Page_Login,
    Page_Danhba,
    Page_Chat,
    Page_Dangki,
    Page_OTP,
    Page_newUser,
    Page_Danhsachnguoidung
};