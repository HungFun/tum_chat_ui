var mongoose = require('mongoose');

var DirectorySchema = mongoose.Schema({
    mysdt:{
        type: String,
        default: 'No Name'
    },
    ten:{
        type: String,
        default: 'No Name'
    },
    sdt:{
        type: String,
        default: 'No Name'
    },
});

module.exports = mongoose.model('directory',DirectorySchema, 'directory');