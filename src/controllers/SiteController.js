var express = require('express');
var router = express.Router();
var user = require('../model/User');
require('dotenv').config()
const avatarController = require('./avata.controller')
var formidable = require('formidable');
var nodemailer = require('nodemailer');

/**
 * 
 * Tạo mail gửi mã otp đi
 */
function Sent_otp_Mail(mail_nhan) {
    console.log(mail_nhan);
    var form = new formidable.IncomingForm();
    // mail server để gửi đi
    const option = {
        service: 'gmail',
        auth: {
            user: 'otp.tumblrz@gmail.com',
            pass: '12345678hihi'
        }
    };
    var transporter = nodemailer.createTransport(option);
    transporter.verify(function(error, success) {
        // Nếu có lỗi.
        if (error) {
            console.log(error);
        } else { //Nếu thành công.
            console.log('Kết nối thành công!');
            var mail = {
                from: 'hungffun.2@gmail.com', // Địa chỉ email của người gửi
                to: mail_nhan, // Địa chỉ email của người nhận
                subject: 'Thư được gửi bằng Sever Tumlrz', // Tiêu đề mail
                // text: 'Xác nhận địa chỉ email của bạn',
                html: " <h3>Xác nhận địa chỉ email của bạn</h3> </br> <p>Trước khi tạo tài khoản Tumblrz, bạn còn cần hoàn thành một bước nhỏ nữa. Hãy chắc chắn rằng đây là địa chỉ email chính xác của bạn  </p> </br> <p> — vui lòng xác nhận đây là địa chỉ chính xác để sử dụng cho tài khoản mới của bạn.</p> </br> <p>Vui lòng nhập mã xác nhận này để bắt đầu trên Tumblrz: </p> </br> <p> " + otpgui + "</p> </br> <p>Mã xác nhận hết hạn sau hai giờ.</p>  </br> <p>Xin cảm ơn</p>  </br> <p>Tumblrz</p>"
            };
            //Tiến hành gửi email
            transporter.sendMail(mail, function(error, info) {
                if (error) { // nếu có lỗi
                    console.log(error);
                } else { //nếu thành công
                    console.log('Email sent: ' + info.response);
                    // gọi sang giao diện otp
                    res.writeHead('200', { 'Content-Type': 'text/html' });
                    fs.readFile('otp', 'utf8', function(err, data) {
                        //nếu nỗi thì thông báo
                        if (err) throw err;
                        //không lỗi thì render data
                        res.end(data);
                    })
                }
            });
        }
    });
}

function between(min, max) {
    return Math.floor(
        Math.random() * (max - min + 1) + min
    )
}
var otpgui = between(800, 900);
var mail_nhan = null;
router.post('/guimail', (req, res) => {
    mail_nhan = req.body.email;
    Sent_otp_Mail(mail_nhan);
    const message = { 'MessageError': String, 'TrangThai': String };
    var mess = Object.create(message);
    mess.MessageError = "Sai mã otp";
    mess.TrangThai = "hidden";
    res.render('otp.ejs', { mess: mess });
});

router.post('/acceptOTP', (req, res) => {
    var otp_nhan = req.body.otp;
    console.log(otpgui)
    console.log(otp_nhan)
    if (otp_nhan == otpgui) {
        res.render('thongtindangkiEmail.ejs', { mail: mail_nhan });
    } else {
        const message = { 'MessageError': String, 'TrangThai': String };
        var mess = Object.create(message);
        mess.MessageError = "Sai mã otp";
        mess.TrangThai = "";
        res.render('otp.ejs', { mess: mess });
    }

});

/**
 * Thêm user và lưu xuống data
 */

router.post('/themuser', async(req, res) => {
    // Lấy hình từ giao diện
    let files = req.files;
    let avatar = await files.avatarNew;
    // Thêm vào s3, lấy linh hình lưu vào avatar
    const uploadS3 = await avatarController.uploadAvatar(avatar);

    let newUser = new user({
        ho: req.body.ho,
        ten: req.body.ten,
        email: req.body.email,
        SDT: req.body.SDT,
        ngaysinh: req.body.ngaysinh,
        gioitinh: req.body.gioitinh,
        matkhau: req.body.matkhau,
        avatar: uploadS3
    });
    newUser.save()
        .then(doc => {
            res.redirect('/');

        })
        .catch(err => {
            console.log('Error: ', err);
            throw err;
        })
});
router.get('/update-user/:tieudeID', (req, res) => {
    let iduser = req.params.tieudeID;
    user.findOne({ _id: iduser }, function(err, user) {
        res.render('updateUser.ejs', { user: user });
    });
});
router.get('/update-userMK/:tieudeID', (req, res) => {
    let iduser = req.params.tieudeID;
    user.findOne({ _id: iduser }, function(err, user) {
        res.render('thaydoimatkhau.ejs', { user: user });
    });
});

// thay doi mat khau

router.post('/thaydoimatkhau:uID', (req, res) => {
    let userID = req.params.uID;
    user.findOne({ _id: userID }, (err, u) => {
        console.log(req.body.matkhaumoi);
        u.matkhau = new String(req.body.matkhaumoi);
        u.save();
        res.redirect('/danhba')
    });
});
router.post('/updateUser/:id', async(req, res) => {
    let id = req.params.id;
    let files = req.files;
    if (files != null) {
        let avatar = await files.avatarNew;
        const uploadS3 = await avatarController.uploadAvatar(avatar);
        user.findByIdAndUpdate({ _id: id }, {
                $set: {
                    ho: req.body.ho,
                    ten: req.body.ten,
                    email: req.body.email,
                    SDT: req.body.SDT,
                    ngaysinh: req.body.ngaysinh,
                    gioitinh: req.body.gioitinh,
                    avatar: uploadS3
                }
            }, { useFindAndModify: false })
            .then(doc => {
                res.redirect('/danhba')
            })
    } else {
        user.findByIdAndUpdate({ _id: id }, {
                $set: {
                    ho: req.body.ho,
                    ten: req.body.ten,
                    email: req.body.email,
                    SDT: req.body.SDT,
                    ngaysinh: req.body.ngaysinh,
                    gioitinh: req.body.gioitinh,
                }
            }, { useFindAndModify: false })
            .then(doc => {
                res.redirect('/danhba')
            })
    }
});
module.exports = router;