var express = require('express');
const session = require('express-session');
var app = require('express')();
var http = require('http').Server(app);
var ejs = require('ejs');
const handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
const path = require('path');
const fileUpload = require('express-fileupload');
const routerLogin = require('./routers/Login.router');
const controller = require('./controllers/SiteController');
const Database = require('./config/data');
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

//app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.engine('hbs', handlebars({
    extname: 'hbs'
}));

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources/views'));

// enable file upload
app.use(fileUpload({
    createParentPath: true,
    limits: { fileSize: 1000 * 1024 * 1024 }
}))

//session
app.use(session({
    resave: true,
    saveUninitialized: true,
    secret: 'somesecret',
    cookie: { maxAge: 60000 }
}));

app.use('/', routerLogin);
app.use(controller);



http.listen(process.env.PORT || 3000, function() {
    console.log('app running PORT 3000');
});