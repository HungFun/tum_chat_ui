const express = require('express')
const router = express.Router()
const Controller = require('../controllers/Login.controller')


// API login
router.get('/', Controller.Page_Login)

//API danh bạ
router.get('/danhba', Controller.Page_Danhba)

//API chat 
router.get('/chat', Controller.Page_Chat)

//API dang ki
router.get('/dangki', Controller.Page_Dangki)
router.get('/otp', Controller.Page_OTP)
router.get('/newUser', Controller.Page_newUser)


module.exports = router